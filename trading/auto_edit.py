import json
import fileinput

conf = {
	'themoon':
        {
            'ALL_sell_only_mode' : 'false',
            'ALL_DCA_enabled' : '-20',
            'enabled' : 'true',
			'ALL_buy_value' : '2'
        },
	'bullish':
        {
            'ALL_sell_only_mode' : 'false',
            'ALL_DCA_enabled' : '-20',
            'enabled' : 'true',
			'ALL_buy_value' : '0.25'
        },
    'sideway':
        {
            'ALL_sell_only_mode' : 'true',
            'ALL_DCA_enabled' : '-20',
            'enabled' : 'false',
			'ALL_buy_value' : '-0.25'
        },
    'bearish':
        {
            'ALL_sell_only_mode' : 'true',
            'ALL_DCA_enabled' : '-20',
            'enabled' : 'false'
        },
    'badbear':
        {
            'ALL_sell_only_mode' : 'true',
            'ALL_DCA_enabled' : '-20',
            'enabled' : 'false'
        }
}

def changeConf(file, status):
    lines = []
    with open(file + '.properties','r') as f:
        

        for line in f.read().splitlines():
            index = line.split(" = ")[0]
            if conf.get(status).get(index, None):
                line = str(index) + ' = ' +str(conf.get(status).get(index))
            lines.append(line)  
    

    with open(file + '.properties','w') as f:
        f.write('\n'.join(lines))




changeConf('PAIRS','sideway')

