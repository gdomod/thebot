{
    "refcoinlimit": 50,
    "waittime": 100,
    "market": "BTC",
    "calculation": "except",
    "whitelist": "",
    "backupfiles": 0,
    "use_telegram": 1,
    "name_telegram": "btc market",

    "themoon_trigger": 3.0,
    "bullish_trigger": 1.5,
    "bearish_trigger": -1.2,
    "badbear_trigger": -3.0,

    "drop_limit": -1.8,
    "drop_periods": 9,

    "themoon_files":
        {
            "bot1_file1": { "from": "moon_PAIRS.properties", "to": "PAIRS.properties" },
            "bot1_file2": { "from": "moon_DCA.properties", "to": "DCA.properties" },
            "bot1_file3": { "from": "moon_INDICATORS.properties", "to": "INDICATORS.properties" }
        },

    "bullish_files":
        {
            "bot1_file1": { "from": "bull_PAIRS.properties", "to": "PAIRS.properties" },
            "bot1_file2": { "from": "bull_DCA.properties", "to": "DCA.properties" },
            "bot1_file3": { "from": "bull_INDICATORS.properties", "to": "INDICATORS.properties" }
        },

    "sideway_files":
        {
            "bot1_file1": { "from": "side_PAIRS.properties", "to": "PAIRS.properties" },
            "bot1_file2": { "from": "side_DCA.properties", "to": "DCA.properties" },
            "bot1_file3": { "from": "side_INDICATORS.properties", "to": "INDICATORS.properties" }
        },

    "bearish_files":
        {
            "bot1_file1": { "from": "bear_PAIRS.properties", "to": "PAIRS.properties" },
            "bot1_file2": { "from": "bear_DCA.properties", "to": "DCA.properties" },
            "bot1_file3": { "from": "bear_INDICATORS.properties", "to": "INDICATORS.properties" }
        },
    "badbear_files":
        {
            "bot1_file1": { "from": "badb_PAIRS.properties", "to": "PAIRS.properties" },
            "bot1_file2": { "from": "badb_DCA.properties", "to": "DCA.properties" },
            "bot1_file3": { "from": "badb_INDICATORS.properties", "to": "INDICATORS.properties" }
        }
}
