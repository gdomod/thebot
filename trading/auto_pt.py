#! python3
# -*- coding: utf-8 -*-


import sys
import time
import datetime

import urllib.request

import os
import shutil

import csv
import json

import auto_edit

def telegram_msg(text):
    global flag_telegram
    if flag_telegram == True:
        try:
            telegram_send.send(messages=[text], conf="./telegram-send.conf")
        except:
            print("\nERROR\nException while sending telegram message")
            print("Therefore telegram usage switched off ...")
            print("(Wrong IDs in telegram configuration file?)\n")
            writelog("\nERROR\nException sending telegram message!\n")
            writelog("Therefore telegram usage switched off ...\n\n")
            flag_telegram = False


def writelog(text):
    mylog = open(logfile, "a")
    mylog.write(text)
    mylog.close()
    return

def writecsvheader(l_header):
    csvfile = open(csvfilename, "a", newline='')
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow(l_header)
    #csvfile.flush()
    csvfile.close()
    
def writecsv(timestamp, nowdate, trend, smode, sinfo):

    if smode == 'BADBEAR':
        imode = -2
    elif smode == 'BEARISH':
        imode = -1
    elif smode == 'SIDEWAY':
        imode = 0
    elif smode == 'BULLISH':
        imode = 1
    elif smode == 'THEMOON':
        imode = 2

    csvfile = open(csvfilename, "a", newline='')
    csvwriter = csv.writer(csvfile)
    csvwriter.writerow([timestamp, nowdate, trend, imode, sinfo])
    #csvfile.flush()
    csvfile.close()


def loadtrendcsv(csvfilename):
    csvdata = []
    csvFile = open(csvfilename)
    csvReader = csv.reader(csvFile)
    for line in csvReader:
        oneline = {}
        oneline['timestamp']   = int(line[0])
        oneline['date']   = line[1]
        oneline['trend']   = float(line[2])
        oneline['range']   = line[3]
        oneline['info']   = line[4]
        csvdata += [oneline]
    csvFile.close()
    return csvdata

def getcsvline(csvdata, idx):
    if idx < len(csvdata):
        onecsv = csvdata[idx]
        timestamp = onecsv['timestamp']
        mydate = onecsv['date']
        trend = onecsv['trend']
        info = onecsv['info']
    else:
        #print("getprice((): index {0:d} out of bounds ...".format(idx))
        timestamp = 0
        mydate = ''
        trend = 0.0
        info = ''
    return (timestamp, mydate, trend, info)


def getticker():
    data_json = ''
    try:
        resp = urllib.request.urlopen("https://api.coinmarketcap.com/v1/ticker/?convert=EUR&limit={0:d}".format(refcoinlimit))
        data = resp.read().decode('utf-8')
        #print(data)
        data_json = json.loads(data)
    except:
        (type, value, traceback) = sys.exc_info()
        print("Exception:\n")
        print(type)
        print(value)
        data_json = ''
    return data_json

def switchonefile(fromfile, tofile):
    #print("  - overwrite '{0:s}' with '{1:s}'".format(tofile, fromfile))
    if dobackups == 1:
        if os.path.exists(tofile):
            shutil.copyfile(tofile, tofile + '_' + mydatef)
            print("  - backup '{0:s}' to '{1:s}'".format(tofile, tofile + '_' + mydatef))
            writelog("  - backup '{0:s}' to '{1:s}'\n".format(tofile, tofile + '_' + mydatef))
    if os.path.exists(fromfile):
        shutil.copyfile(fromfile, tofile)
        os.utime(tofile)
        print("  - replacing file '{0:s}' by '{1:s}'".format(tofile, fromfile))
        writelog("  - replacing file '{0:s}' by '{1:s}'\n".format(tofile, fromfile))
    return

def switchfiles(myconfig, smode):
    smode = smode.lower()

    filelist = ('DCA', 'PAIRS')
    for file in filelist:
        auto_edit.changeConf(file, smode)

def addchg(all_chg1h, newchg):
    all_chg1h += [newchg]
    if len(all_chg1h) > max_chg1h_count:
        all_chg1h = all_chg1h[1:]

def checkchg(all_chg1h, drop_limit, drop_period):
    drop = 0.0
    count = min(drop_period, len(all_chg1h) - 1)
    for i in range(count):
        drop += all_chg1h[-1 - i] - all_chg1h[-2 - i]
        if drop < drop_limit:
            break
    return drop



# ---------------------------------------------------------------------------
#
# Config Functions
#
# ---------------------------------------------------------------------------

def cfg_loadconfig(filename):
    configFile = open(filename)
    stringJSON = configFile.read()
    configFile.close()
    myconfig = json.loads(stringJSON)
    return myconfig

def cfg_gettriggers(myconfig):
    badbtrigger = float(myconfig['badbear_trigger'])
    beartrigger = float(myconfig['bearish_trigger'])
    bulltrigger = float(myconfig['bullish_trigger'])
    moontrigger = float(myconfig['themoon_trigger'])
    return (badbtrigger, beartrigger, bulltrigger, moontrigger)

def cfg_getdropparams(myconfig):
    drop_limit = float(myconfig['drop_limit'])
    drop_period = int(myconfig['drop_periods'])
    return (drop_limit, drop_period)

def cfg_chkfilenames(myconfig):
    status = 0
    for smode in ['badbear', 'bearish', 'sideway', 'bullish', 'themoon']:
        filelist = myconfig[smode + '_files']
        #print(filelist)
        for onefilename in filelist:
            onefile = filelist[onefilename]
            #print(onefile)
            if ('from' in onefile) and ('to' in onefile):
                fromfile = onefile['from']
                tofile   = onefile['to']
                if not os.path.exists(fromfile):
                    print("Section '{0:s}' - 'from' file '{1:s}' not found!".format(smode + '_files', fromfile))
                    status += 1
                if not os.path.exists(tofile):
                    print("Section '{0:s}' - 'to' file '{1:s}' not found!".format(smode + '_files', tofile))
                    status += 1
    if status == 0:
        print("ok - all files of config 'auto_pt.js' have been found ...")
    else:
        print("error - {0:d} files of files sections of 'auto_pt.js' are missing ...".format(status))
    return status


# ---------------------------------------------------------------------------------
#
#  M A I N
#
# ---------------------------------------------------------------------------------


flag_telegram = False
try:
    import telegram_send
    flag_telegram = True
except:
    (type, value, traceback) = sys.exc_info()
#    print(type)
#    print(value)
#    print(traceback)


scriptversion = 'V0.5 (Stable)'
csvfilename = 'auto_pt.csv'
logfile = 'auto_pt.log'
botconfig = 'auto_pt.js'
all_chg1h = []
max_chg1h_count = 24
adrop = 0
csv_idx = 0
# variables for testmode
testmode = 0
testcsvdata = []

# start mode -99 can never happen
# so the script switches already at startup to a defined and correct mode
mmode = -99


time.sleep(10)


if (len(sys.argv) > 1):
    if (sys.argv[1] == '-t'):
        testmode = 1
        print("Setting test mode ...")
        csvtestfile = csvfilename
        startTime = time.time()
        adate = datetime.datetime.fromtimestamp(startTime)
        mytime = adate.strftime('%H%M%S')
        csvfilename = csvfilename.replace('.csv', '.' + mytime + '.csv')
        if (len(sys.argv) > 2):
            csvtestfile = sys.argv[2]
        print("\nSwitching to testmode ...")
        print("- loading test data from file '{0:s}'".format(csvtestfile))
        print("- saving result data to file '{0:s}'".format(csvfilename))
        testcsvdata = loadtrendcsv(csvtestfile)
        print()
        time.sleep(3)
    else:
        botconfig = sys.argv[1]
        
print('\nLoading auto_pt config file ' + botconfig)
writelog('\nLoading auto_pt config file ' + botconfig + '\n')

if not os.path.exists(botconfig):
    print("\nError - config file '{0:s}' not found ...\n".format(botconfig))
    time.sleep(5)
    exit()

try:
    myconfig = cfg_loadconfig(botconfig)
except:
    (type, value, traceback) = sys.exc_info()
    print(type)
    print(value)
    print("\nError in config 'auto_pt.js' - check syntax (commas, brackets, ...!) ...")
    time.sleep(5)
    exit()

refcoinlimit = int(myconfig['refcoinlimit'])
waittime = int(myconfig['waittime'])
whitelist = myconfig['whitelist']
market = myconfig['market'].upper()
if (market != 'USDT') and (market != 'BTC') and (market != 'ETH') and (market != 'XMR') :
    print("Error in config 'auto_pt.js' - supported markets only are 'USDT', 'BTC', 'ETH', XMR' ...")
    time.sleep(5)
    exit()
calculation = myconfig['calculation'].lower()
if (calculation != 'except') and (calculation != 'diff'):
    print("Error in config 'auto_pt.js' - supported calculation methods only are 'except' and 'diff' ...")
    time.sleep(5)
    exit()
dobackups = int(myconfig['backupfiles'])
use_telegram  = int(myconfig['use_telegram'])
name_telegram  = myconfig['name_telegram']

if testmode == 1:
    # change variables which have no sense in testmode
    use_telegram = 0
    flag_telegram = False
    whitelist = 'see original csv'
    calculation = 'see original csv'

       

(badbtrigger, beartrigger, bulltrigger, moontrigger) = cfg_gettriggers(myconfig)
(drop_limit, drop_period) = cfg_getdropparams(myconfig)

if testmode == 0:
    print("top coins to test:     {0:d}".format(refcoinlimit))
    print("update trend:          {0:d} sec".format(waittime))
    print("market:                {0:s}".format(market))
    print("calculation method:    {0:s}".format(calculation))
    print("coin whitelist:        {0:s}".format(whitelist if (whitelist != '') else '-- none --'))
    print("backup files:          {0:s}".format('yes' if (dobackups == 1) else 'no'))
    print("use telegram:          {0:s}".format('yes' if (use_telegram == 1) else 'no'))
    print("themoon trigger:      {0:7.4f}".format(moontrigger))
    print("bullish trigger:      {0:7.4f}".format(bulltrigger))
    print("bearish trigger:      {0:7.4f}".format(beartrigger))
    print("badbear trigger:      {0:7.4f}".format(badbtrigger))
    print("drop limit:           {0:7.4f}".format(drop_limit))
    print("drop periods:          {0:d}".format(drop_period))
    print()

    writelog("top coins to test:     {0:d}\n".format(refcoinlimit))
    writelog("update trend:          {0:d} sec\n".format(waittime))
    writelog("Market:                {0:s}\n".format(market))
    writelog("calculation method:    {0:s}\n".format(calculation))
    writelog("coin whitelist:        {0:s}\n".format(whitelist if (whitelist != '') else '-- none --'))
    writelog("backup files:          {0:s}\n".format('yes' if (dobackups == 1) else 'no'))
    writelog("use telegram:          {0:s}\n".format('yes' if (use_telegram == 1) else 'no'))
    writelog("the moon trigger:     {0:7.4f}\n".format(moontrigger))
    writelog("bullish trigger:      {0:7.4f}\n".format(bulltrigger))
    writelog("bearish trigger:      {0:7.4f}\n".format(beartrigger))
    writelog("badbear trigger:      {0:7.4f}\n".format(badbtrigger))
    writelog("drop limit:           {0:7.4f}\n".format(drop_limit))
    writelog("drop periods:          {0:d}\n".format(drop_period))
    writelog("\n")

    if flag_telegram == True:
        telegram_msg("{0:s}  -  startup of auto_pt script".format(name_telegram))
        print()
    else:
        print("no telegram support found ...")
        print("check readme.txt to enable telegram ...")
        print()

    # telegram support wanted?
    if use_telegram != 1:
        flag_telegram = False

counter = 0
COUNTER_LIMIT = 10
while 1:
    
    if testmode == 0:
        startTime = time.time()
        adate = datetime.datetime.fromtimestamp(startTime)
        mydate = adate.strftime('%Y%m%d_%H:%M:%S')
        mydatef = adate.strftime('%Y%m%d_%H%M%S')

        data_json = getticker()
        if data_json == '':
            # no connection to conmarketcap
            time.sleep(waittime)
            continue
    else:
        (startTime, mydatef, chg1h, s_info) = getcsvline(testcsvdata, csv_idx)
        if startTime == 0:
            break # end of test data reached
        print("{0:s} - trend {1:5.2f}".format(mydatef, chg1h))
        addchg(all_chg1h, chg1h)


    if testmode == 0:

        # get the current 1h trend
        allchg1h = chg1h = refchg1h = count = 0.0
        for onecoin in data_json:
            coin = onecoin['symbol']
            if ('symbol' in onecoin) and ('percent_change_1h' in onecoin):
                try:
                    pchange1h = float(onecoin['percent_change_1h'])
                    if coin == market:
                        refchg1h = pchange1h
                        allchg1h += pchange1h
                        continue
                    if (whitelist != '') and not (coin in whitelist):
                        continue
                    chg1h += float(onecoin['percent_change_1h'])
                    allchg1h += float(onecoin['percent_change_1h'])
                    count += 1
                except:
                    (type, value, traceback) = sys.exc_info()
        # get the average change
        if count > 0:
            chg1h /= count
            allchg1h /= count + 1
        else:
            chg1h = 0.0
            allchg1h = 0.0
            
        print("chg_all: {0:6.3f}, chg_except: {1:6.3f}, chg_diff {2:6.3f}  (refchg({3:s}): {4:6.3f})".format(allchg1h, chg1h, chg1h-refchg1h, market, refchg1h))

        if calculation == 'except':
            addchg(all_chg1h, chg1h)
        elif calculation == 'diff':
            chg1h = chg1h - refchg1h
            addchg(all_chg1h, chg1h)

    # keep an eye for a sudden drop
    drop = checkchg(all_chg1h, drop_limit, drop_period)
    if drop < drop_limit:
        # drop overwrites 1h trend
        ori_chg1h = chg1h
        chg1h = -0.0001 + badbtrigger
        if adrop == 0:
            if testmode == 0:
                print("{0:s}  -  drop limit exceeded!".format(mydate))
                writelog("{0:s}  -  drop limit exceeded!\n".format(mydate))
                telegram_msg("{0:s} - {1:s}  -  drop limit exceeded!".format(mydate, name_telegram))

        adrop = 1
    else:
        adrop = 0

    if chg1h > moontrigger:
        counter = counter + 1
        smode = 'THEMOON'
        if mmode != 2 and counter > COUNTER_LIMIT:
            mmode = 2
           
            if testmode == 0:
                print("- switching to {0:s} mode ...".format(smode))
                writelog("- switching to {0:s} mode ...\n".format(smode))
                telegram_msg("{0:s} - {1:s} - switching to {2:s} mode ...".format(mydate, name_telegram, smode))
                switchfiles(myconfig, smode)
    elif chg1h > bulltrigger:
        counter = counter + 1
        smode = 'BULLISH'
        if mmode != 1  and counter > COUNTER_LIMIT:
            mmode = 1

            
            if testmode == 0:
                print("- switching to {0:s} mode ...".format(smode))
                writelog("- switching to {0:s} mode ...\n".format(smode))
                telegram_msg("{0:s} - {1:s} - switching to {2:s} mode ...".format(mydate, name_telegram, smode))
                switchfiles(myconfig, smode)
    elif chg1h < badbtrigger:
        counter = 0
        if mmode != -2:
            mmode = -2
            smode = 'BADBEAR'
            counter = 0
            if testmode == 0:
                print("- switching to {0:s} mode ...".format(smode))
                writelog("- switching to {0:s} mode ...\n".format(smode))
                telegram_msg("{0:s} - {1:s} - switching to {2:s} mode ...".format(mydate, name_telegram, smode))
                switchfiles(myconfig, smode)
    elif chg1h < beartrigger:
        counter = 0
        if mmode != -1:
            mmode = -1
            smode = 'BEARISH'
            
            if testmode == 0:
                print("- switching to {0:s} mode ...".format(smode))
                writelog("- switching to {0:s} mode ...\n".format(smode))
                telegram_msg("{0:s} - {1:s} - switching to {2:s} mode ...".format(mydate, name_telegram, smode))
                switchfiles(myconfig, smode)
    else:
        counter = 0
        if mmode != 0:
            mmode = 0
            smode = 'SIDEWAY'
            if testmode == 0:
                print("- switching to {0:s} mode ...".format(smode))
                writelog("- switching to {0:s} mode ...\n".format(smode))
                telegram_msg("{0:s} - {1:s} - switching to {2:s} mode ...".format(mydate, name_telegram, smode))
                switchfiles(myconfig, smode)

    if testmode == 0:
        if adrop == 1:
            print("{0:s}  -  {1:s}  -  drop by {2:6.3f}".format(mydate, "DROP!  ", drop))
            print("{0:s}  -  {1:s}  -  chg_1h {2:6.3f}".format(mydate, smode + str(counter), ori_chg1h))
            writelog("{0:s}  -  {1:s}  -  drop by {2:6.3f}\n".format(mydate, "DROP!  ", drop))
            writelog("{0:s}  -  {1:s}  -  chg_1h {2:6.3f}\n".format(mydate, smode + str(counter), ori_chg1h))
        else:
            print("{0:s}  -  {1:s}  -  chg_1h {2:6.3f}".format(mydate, smode + str(counter), chg1h))
            writelog("{0:s}  -  {1:s}  -  chg_1h {2:6.3f}\n".format(mydate, smode + str(counter), chg1h))


    # write additional infos into csv
    if csv_idx == 0:
        s_info = "Config-Params"
    elif csv_idx == 1:
        s_info = "Scriptversion: {0:s}".format(scriptversion)
    elif csv_idx == 2:
        s_info = "refcoinlimit: {0:d}".format(refcoinlimit)
    elif csv_idx == 3:
        s_info = "waittime: {0:d}".format(waittime)
    elif csv_idx == 4:
        s_info = "market: {0:s}".format(market)
    elif csv_idx == 5:
        s_info = "calculation: {0:s}".format(calculation)
    elif csv_idx == 6:
        s_info = "whitelist: {0:s}".format(whitelist)
    elif csv_idx == 7:
        s_info = "themoon_trigger: {0:5.2f}".format(moontrigger)
    elif csv_idx == 8:
        s_info = "bullish_trigger: {0:5.2f}".format(bulltrigger)
    elif csv_idx == 9:
        s_info = "bearish_trigger: {0:5.2f}".format(beartrigger)
    elif csv_idx == 10:
        s_info = "badbear_trigger: {0:5.2f}".format(badbtrigger)
    elif csv_idx == 11:
        s_info = "drop_limit: {0:5.2f}".format(drop_limit)
    elif csv_idx == 12:
        s_info = "drop_periods: {0:d}".format(drop_period)
    else:
        s_info = ''
    csv_idx += 1
    #writecsv(int(startTime), mydatef, chg1h if (adrop == 0) else ori_chg1h, smode, s_info)

    if testmode == 0:
        sys.stdout.flush()
        time.sleep(waittime)
