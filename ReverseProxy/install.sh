cp /root/application.properties /tmp
cp /root/ProfitTrailerData.json /tmp
sed -i s/server.port\ =\ 8081/server.port\ =\ 15327/g /root/application.properties
sed -i s/server.enableConfig\ =\ false/server.enableConfig\ =\ true/g /root/application.properties

npm install .
ln reverseproxy.service /etc/systemd/system/reverseproxy.service
systemctl enable reverseproxy.service
systemctl daemon-reload

ln /root/trading/markettrend.service /etc/systemd/system/markettrend.service
systemctl enable markettrend.service
systemctl daemon-reload


sleep 2
service bot stop
sleep 5
service bot start
service reverseproxy start 