var http = require('http');
var httpProxy = require('http-proxy');
var modifyResponse = require('http-proxy-response-rewrite');

// Create a proxy server
var proxy = httpProxy.createProxyServer({
    target: 'http://127.0.0.1:15327'
});

proxy.on( 'error', function( error ){
    console.log( error );
});

// Listen for the `proxyRes` event on `proxy`.
proxy.on('proxyRes', function (proxyRes, req, res) {
    modifyResponse(res, proxyRes.headers['content-encoding'], function (body) {
        if (body) {
            // modify some information
            var header = /<h1(.|\s)*?<\/h1>/gm;
            var footer = /<footer(.|\s)*?<\/footer>/gm;
            //var config = /<li.*?configMenu(.|\s)*<\/li>/gm;
            body =body.replace(header,'');
            body =body.replace(footer,'');
            body =body.replace(/<title.*?ProfitTrailer.*?<\/title>/gm,'');
            body =body.replace(/<meta name.*?\">/gm,'');
            body =body.replace(/<a class=\"logo\"(.|\s)*?<\/a>/gm,'');
            body =body.replace(/<li.*?configMenu(.|\s)*?<\/ul>/gm,'');
        }
        return body;
    });
});

// Create your server and then proxies the request
var server = http.createServer(function (req, res) {
    proxy.web(req, res);
}).listen(8081);

